import { Meteor } from 'meteor/meteor';

import { Mongo } from 'meteor/mongo';
import { Releases } from '/imports/api/releases.js';

Meteor.methods({
  'initFindbugs':function(){
    Releases.find().forEach(function(release){
      Meteor.call('parseFindbugsXML', release.build);
    });
  },
  'initReleases':function(){
    if(Releases.find().count() < 1){
      console.log('populating releases tables...');

      var releases = Assets.getText("releases.json", function(error, result){
        if(error){
          console.log("error", error);
        }
        if(result){
            JSON.parse(result).forEach(function(release){
              Releases.insert(release);
            });
        }
      });
    }
  }
});

Meteor.startup(() => {
  Meteor.call('initReleases');
  Meteor.call('initFindbugs');
});
