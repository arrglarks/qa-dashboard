import { Meteor } from 'meteor/meteor';

import { Mongo } from 'meteor/mongo';
import { Findings } from '/imports/api/findbugs/findings.js';

Meteor.methods({
  'parseFindbugsXML':function(build){
    if(Findings.find().count() < 1){
      console.log('populating findbugs tables for build ' + build + '...');

      var content = Assets.getText(build + '/FindbugsResult.xml', function(error, result){
        if(error){
          console.log("assets error", error);
        }
        if(result){
          xml2js.parseString(result, {explicitArray:false, emptyTag:undefined}, function (jsError, jsResult) {
            if(jsError){
              console.error('xml2js error',jsError);
            }else{
              jsResult.BugCollection.BugInstance.forEach(function(bug){

                var bugClass = "";
                if(bug.Class.length > 1){
                  bugClass = bug.Class[0].$.classname;
                }else{
                  bugClass = bug.Class.$.classname;
                }

                var finding = {};
                finding.build = build;
                finding.type = bug.$.type;
                finding.priority = bug.$.priority;
                finding.classname = bugClass;

                Findings.insert(finding);
              });
            }
          });
        }
      });
    }
  }
});
