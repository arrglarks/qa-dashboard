# README

This is a Dashboard for the QA Tooling of CEC using Meteor, React and Bootstrap

---

# FEATURES

## Findbugs

  * Number of high and medium priority errors per package

## Argus

  * Number and criticality of findings per repository

## SDK Checker

*coming soon*

---

# SETUP

    meteor npm install
    meteor npm install --save react react-dom
    meteor npm install --save react-addons-pure-render-mixin
    meteor add react-meteor-data
    meteor npm install --save react-bootstrap
    meteor add universe:react-bootstrap
    meteor add peerlibrary:xml2js

## REMOVE LATER

    meteor add autopublish

---

# DATA STRUCTURE

## RELEASES

    {
      "version": "171",
      "milestone": "17.1 RC1",
      "releaseDate": "20170120",
      "build": "R20170113-190225"
    }

## ARGUS

    {
      "build": "R20170113-190225",
      "summary": {
        "CRITICAL": {
          "total": 0,
          "baseline": 0,
          "repos": {
            "CEC": 0,
            "CECUSP": 0,
            "PACE": 0,
            "PACEUSP": 0
          }
        },
        "MAJOR": {
          "total": 39,
          "baseline": 0,
          "repos": {
            "CEC": 6,
            "CECUSP": 5,
            "PACE": 14,
            "PACEUSP": 14
          }
        },
        "MINOR": {
          "total": 171,
          "baseline": 0,
          "repos": {
            "CEC": 84,
            "CECUSP": 50,
            "PACE": 35,
            "PACEUSP": 2
          }
        }
      }
    }
## Findbugs

    {
      "build": "R20170113-190225",
      "type": "NP_LOAD_OF_KNOWN_NULL_VALUE",
      "priority": "2",
      "classname": "com.daimler.pace.cc.source.actions.TypedCopyAction"
    }
