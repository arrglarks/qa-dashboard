import { Mongo } from 'meteor/mongo';

export const Findings = new Mongo.Collection('findbugs_findings');
