import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Nav, Navbar, NavItem, Table } from 'react-bootstrap';

import { Releases } from '../api/releases.js';
import Release from './Release.jsx';
// import { Summaries } from '../api/findbugs/summaries.js';
// import Summary from './findbugs/Summary.jsx';

class App extends Component {
  renderReleases() {
    return this.props.releases.map((release) => (
      <Release key={release._id} release={release} />
    ));
  }

  renderSummaries() {
    // return this.props.summaries.map((summary) => (
    //   <Summary key={summary._id} summary={summary} />
    // ));
  }

  render() {
    return (
      <div className="container">
        <header>
          <h1>CEC QA Dashboard</h1>
        </header>
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              <a href="#">Release</a>
            </Navbar.Brand>
          </Navbar.Header>
          <Nav>
            {this.renderReleases()}
          </Nav>
        </Navbar>
        <section>
          <h2>Findbugs</h2>
          <Table striped bordered condensed hover>
            <thead>
              <tr>
                <th>Package</th>
                <th>High Priority</th>
                <th>Medium Priority</th>
              </tr>
            </thead>
            <tbody>
              {this.renderSummaries()}
            </tbody>
          </Table>
        </section>
      </div>
    );
  }
}

App.propTypes = {
  releases: PropTypes.array.isRequired
};

export default createContainer(() => {
  return {
    releases: Releases.find({}).fetch()
  };
}, App);
